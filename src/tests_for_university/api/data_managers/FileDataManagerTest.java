package tests_for_university.api.data_managers;

import static org.junit.jupiter.api.Assertions.*;
import static tests_for_university.Constants.*;

import java.io.IOException;
import java.nio.file.*;

import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.luxoft.university.api.data_managers.FileDataManager;
import com.luxoft.university.api.exceptions.FileDataManagerException;
import com.luxoft.university.api.models.GroupNode;
import static com.luxoft.university.api.utils.NodeUtilities.*;

public class FileDataManagerTest {
	@Mock
	private GroupNode mockNode;
	private FileDataManager manager;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
		manager = new FileDataManager();
	}

	@AfterAll
	static void dispose() {
		try {
			Files.deleteIfExists(Path.of(PATH_STRING));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(ERROR_DELETING_FILE);
		}
	}

	@Test
	@DisplayName("When save entity and load it back then receive equal objects")
	void whenSaveEntityAndLoadItThenReceiveEqualObjects() {
		try {
			GroupNode expectedNode = createGroupNode(NEW_NAME, null);
			manager.saveData(PATH_STRING, expectedNode);
			GroupNode actualNode = manager.loadData(PATH_STRING);
			assertEquals(expectedNode.getId(), actualNode.getId());
			assertEquals(expectedNode.getName(), actualNode.getName());
		} catch (FileDataManagerException e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("When pass null as Path to saveData() then receive NPE")
	void whenPassNullAsPathToSaveDataThenReceiveNPE() {
		assertThrows(NullPointerException.class, () -> manager.saveData(null, mockNode));
	}

	@Test
	@DisplayName("When pass null as Path to loadData() then receive NPE")
	void whenPassNullAsPathToLoadDataThenReceiveNPE() {
		assertThrows(NullPointerException.class, () -> manager.loadData(null));
	}

	@Test
	@DisplayName("When pass null as GroupNode in saveData() then receive FileDataManagerException")
	void setNullAsGroupNodeWhenCallSaveMethod() {
		assertThrows(FileDataManagerException.class, () -> manager.saveData(PATH_STRING, null));
	}

	@Test
	@DisplayName("When pass wrong file name then receive FileDataManagerException")
	void passWrongFileThenReceiveException() {
		assertThrows(FileDataManagerException.class, () -> manager.loadData("WRONG FILE NAME"));
		assertThrows(FileDataManagerException.class, () -> manager.saveData("DD:\\!?>./", mockNode));
	}
}
