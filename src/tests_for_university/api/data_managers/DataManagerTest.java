package tests_for_university.api.data_managers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.*;
import org.mockito.*;

import com.luxoft.university.api.data_managers.DataManager;
import com.luxoft.university.api.models.GroupNode;

public class DataManagerTest {
	@Mock
	private DataManager mockManager;
	

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When call createRoot() then receive new GroupNode object with initialized fields")
	void whenCallCreateRootThenRecieveBlankObject() {
		when(mockManager.createRoot()).thenCallRealMethod();
		GroupNode groupNode = mockManager.createRoot();
		assertNotNull(groupNode);
		assertFalse(groupNode.getName().isEmpty());
		assertNotNull(groupNode.getEntries());
	}
}
