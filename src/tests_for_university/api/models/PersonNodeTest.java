package tests_for_university.api.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

import com.luxoft.university.api.models.*;

import nl.jqno.equalsverifier.EqualsVerifier;
import static com.luxoft.university.api.utils.NodeUtilities.*;

public class PersonNodeTest {
	private PersonNode personJohn;
	private PersonNode personMike;
	private GroupNode nodeA;
	private GroupNode nodeB;
	private GroupNode nodeC;
	private GroupNode nodeD;

	
	@BeforeEach
	void init() {
		nodeA = createGroupNode("a", null);
		nodeB = createGroupNode("b", nodeA);
		nodeC = createGroupNode("c", nodeB);
		nodeD = createGroupNode("d", nodeC);

		personJohn = createPersonNode("John", nodeD, "Shevchenko str.", "Dnipro", 5, "c:\1.jpg");
		personMike = createPersonNode("Mike", nodeB, "address", "city", -1, "file");
	}

	@Test
	@DisplayName("When test updateState() and getters all goes fine")
	void testingGettersAndSetters() {
		personJohn.udateState(
				personMike.getName(), 
				personMike.getParent().getName(), 
				personMike.getAddress(),
				personMike.getCity(), 
				personMike.getResult(), 
				personMike.getPhoto()
				);

		assertEquals(personMike.getName(), personJohn.getName());
		assertEquals(personMike.getParent().getId(), personJohn.getParent().getId());
		assertEquals(personMike.getAddress(), personJohn.getAddress());
		assertEquals(personMike.getCity(), personJohn.getCity());
		assertEquals(personMike.getResult(), personJohn.getResult());
	}

//	@Test
//	@DisplayName("Testing equals with EqualsVerifier")
//	public void equalsContract() {
//		EqualsVerifier.simple().forClass(PersonNode.class).withPrefabValues(GroupNode.class, nodeA, nodeB).verify();
//	}
}
