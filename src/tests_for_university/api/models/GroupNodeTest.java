package tests_for_university.api.models;

import static org.junit.jupiter.api.Assertions.*;

import static tests_for_university.Constants.*;

import org.junit.jupiter.api.*;

import com.luxoft.university.api.models.*;
import static com.luxoft.university.api.utils.NodeUtilities.*;

public class GroupNodeTest {
	private GroupNode nodeA;
	private GroupNode nodeB;
	private GroupNode nodeC;
	private GroupNode nodeD;

	@BeforeEach
	void init() {
		nodeA = createGroupNode("a", null);
		nodeB = createGroupNode("b", nodeA);
		nodeC = createGroupNode("c", nodeB);
		nodeD = createGroupNode("d", nodeC);
	}

	@Test
	@DisplayName("When call updateState then parent, child and ex-parent are updated")
	void whenCallUpdateState() {
		nodeD.updateState(NEW_NAME, nodeB.getName());

		assertEquals(NEW_NAME, nodeD.getName());
		assertTrue(verifyParent(nodeB, nodeD).isPresent());
		assertTrue(verifyParent(nodeC, nodeD).isEmpty());
	}

	@Test
	@DisplayName("When call detachFromParent then entitiy is removed from parent's entries list")
	void whenCallDetachFromParent() {
		nodeB.detachFromParent();
		assertTrue(verifyParent(nodeA, nodeB).isEmpty());
	}

	@Test
	@DisplayName("When call addChild method receive correct result")
	void testingAddChildMethod() {
		nodeA.addChild(nodeC);
		assertTrue(verifyParent(nodeA, nodeC).isPresent());
	}

	@Test
	@DisplayName("Testing getters and setters")
	void testingGettersAndSetters() {
		nodeA.setName(NEW_NAME);
		assertEquals(NEW_NAME, nodeA.getName());
	}
}
