package tests_for_university.api.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

import com.luxoft.university.api.models.*;

import static tests_for_university.Constants.*;
import static com.luxoft.university.api.utils.NodeUtilities.*;

public class BaseNodeTest {
	private PersonNode person;
	private GroupNode nodeA;
	private GroupNode nodeB;
	private GroupNode nodeC;
	private GroupNode nodeD;

	@BeforeEach
	void init() {
		nodeA = createGroupNode("a", null);
		nodeB = createGroupNode("b", nodeA);
		nodeC = createGroupNode("c", nodeB);
		nodeD = createGroupNode("d", nodeC);
		person = createPersonNode("John", nodeA, "Shevchenko str.", "Dnipro", 5, "c:\1.jpg");
	}

	@Test
	@DisplayName("When updateState with parent that not exists than parent is created")
	void testCreateParentAndBind() {
		person.udateState(
				person.getName(), 
				NEW_NAME, 
				person.getAddress(), 
				person.getCity(),
				person.getResult(), 
				person.getPhoto()
				);
		assertEquals(NEW_NAME, person.getParent().getName());
	}

	@Test
	@DisplayName("When test getDepth method all goes fine")
	void getDepthTest() {
		GroupNode nodeX = createGroupNode(NEW_NAME, nodeA);

		assertEquals(nodeA.getDepth() + 1, nodeX.getDepth());
	}
	
	@Test
	@DisplayName("When call getRoot() recieve root element")
	void getRootTest() {
		assertEquals(nodeA, nodeB.getRoot());
		assertEquals(nodeA, nodeC.getRoot());
		assertEquals(nodeA, nodeD.getRoot());
		assertEquals(nodeA.toString(), nodeD.getRoot().toString());
	}
	
	@Test
	@DisplayName("When call updateParent() the parent is updated")
	void updateParentTest() {
		nodeD.updateParent(nodeB);
		assertEquals(nodeB, nodeD.getParent());
		assertEquals(nodeB.getId(), nodeD.getParent().getId());
	}
}
