package tests_for_university.api.utils;

import static com.luxoft.university.api.utils.NodeUtilities.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;
import org.junit.jupiter.api.*;
import com.luxoft.university.api.models.*;


public class NodeUtilitiesTest {
	private GroupNode nodeA;
	private GroupNode nodeB;
	private GroupNode nodeC;
	private GroupNode nodeD;
	private PersonNode personE;

	
	@BeforeEach
	void init() {
		nodeA = createGroupNode("a", null);
		nodeB = createGroupNode("b", nodeA);
		nodeC = createGroupNode("c", nodeB);
		nodeD = createGroupNode("d", nodeC);
		personE = createPersonNode("personE", nodeA, "Peremogi str.", "KyivE", -1, "c:\\photo\\1.jpg");
	}

	@Test
	@DisplayName("When create blankGroup new group with empty name and given parent is created")
	void createBlankGroupTest() {
		GroupNode newGroup = createBlankGroup(nodeA);
		assertEquals("", newGroup.getName());
		assertEquals(nodeA, newGroup.getParent());
	}
	
	@Test
	@DisplayName("When create blankPerson new person with empty fields and given parent is created")
	void createBlankPersonTest() {
		PersonNode newPerson= createBlankPerson(nodeA);
		assertEquals("", newPerson.getName());
		assertEquals(nodeA, newPerson.getParent());
		assertEquals("", newPerson.getAddress());
		assertEquals("", newPerson.getCity());
		assertEquals("", newPerson.getPhoto());
		assertEquals(-1, newPerson.getResult());
	}	
	
	@Test
	@DisplayName("When testing getAllChilds() then recieve all childs of given element")
	void getAllChildsTest() {
		List<BaseNode> actualList = getAllChilds(nodeA);
		List<BaseNode> expectedList = new ArrayList<BaseNode>(List.of(nodeA, nodeB, nodeC, nodeD, personE));
		
		assertNotEquals(actualList.size(), 0);
		assertEquals(expectedList.size(), actualList.size());
		
		while (actualList.size() != 0) {
			BaseNode node = actualList.get(0);
			assertEquals(expectedList.remove(node), actualList.remove(node));
		}
		
		assertEquals(0, expectedList.size());
		assertEquals(0, actualList.size());
	}
	
	@Test
	@DisplayName("When call getNodeById() the node is founded in tree")
	void getNodeByIdTest() {
		String nodeCId = nodeC.getId();
		BaseNode expected = getNodeById(nodeA, nodeCId);
		
		assertTrue(expected instanceof GroupNode);
		assertEquals(nodeCId, expected.getId());
	}
	
	@Test
	@DisplayName("When call verifyRelationship all goes fine")
	void verifyRelationshipTest() {
		assertTrue(verifyRelationship(nodeA, nodeB));
		assertTrue(verifyRelationship(nodeA, nodeC));
		assertTrue(verifyRelationship(nodeA, nodeD));
		
		GroupNode node = createBlankGroup(null);
		
		assertFalse(verifyRelationship(nodeA, node));
	}
}
