package tests_for_university.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.*;
import org.mockito.*;

import com.luxoft.university.api.UniverService;
import com.luxoft.university.api.data_managers.DataManager;
import com.luxoft.university.api.exceptions.*;
import com.luxoft.university.api.models.GroupNode;

import static tests_for_university.Constants.*;


public class UniverServiceTest {
	@Mock
	private GroupNode mockGroupNode;
	@Mock
	private DataManager mockManager;
	private UniverService service;


	@BeforeEach
	void setUp() throws FileDataManagerException {
		MockitoAnnotations.openMocks(this);
		service = new UniverService(mockManager);
	}

	@Test
	@DisplayName("When call createRoot() then mockManager.createRoot() is called")
	void testCreateRoot() {
		when(mockManager.createRoot()).thenReturn(mockGroupNode);
		GroupNode actualGroupNode = service.createRoot();
		Mockito.verify(mockManager).createRoot();
		assertEquals(mockGroupNode, actualGroupNode);
	}

	@Test
	@DisplayName("When call loadData() then mockManager.loadData() is called")
	void testLoadData() {
		try {
			when(mockManager.loadData(SOURCE)).thenReturn(mockGroupNode);
			GroupNode actualGroupNode = service.loadData(SOURCE);
			Mockito.verify(mockManager).loadData(SOURCE);
			assertEquals(mockGroupNode, actualGroupNode);
		} catch (DataManagerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("When call saveData() then mockManager.saveData() is called")
	void testSaveData() {
		try {
			service.saveData(SOURCE, mockGroupNode);
			Mockito.verify(mockManager).saveData(SOURCE, mockGroupNode);
		} catch (DataManagerException e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("When pass null to loadData() then don't receive any exception")
	void whenPassNulltoLoadDataServiceContinuesToWork() {
		try {
			service.loadData(null);
			verify(mockManager).loadData(null);
		} catch (DataManagerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@DisplayName("When pass null to saveData() then don't receive any exception")
	void whenPassNulltoSaveDataServiceContinuesToWork() {
		try {
			service.saveData(null, null);
			verify(mockManager).saveData(null, null);
		} catch (DataManagerException e) {
			e.printStackTrace();
		}
	}
}
