package tests_for_university;


public final class Constants {
	public static final String NEW_NAME = "New name that not exists";
	public static final String SOURCE = "URI / file name / smth else that is supported";
	public static final String PATH_STRING = "serialize_ckeck.tmp";
	public static final String ERROR_DELETING_FILE = "Error deleting temp file";

	private Constants() {
	}
}
